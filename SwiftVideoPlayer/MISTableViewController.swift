//
//  MISTableViewController.swift
//  VideoPlayer_swift
//
//  Created by Istvan Szabo on 2016. 04. 24.
//  Copyright (c) 2016. Istvan Szabo. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SenseKit
import MediaPlayer

/////////////////////Customize your data url path/////////////////////////////

let URL_DATA = "http://dev.nexeven.se/AgentValidation/iOS/assetList.plist"

//////////////////////////////////////////////////////////////////////////////


var videoDict : NSDictionary?
var videoArray = NSArray()

let NEXEVEN_SERVER = "https://sense.nexeven.io";
let LOCAL_SERVER = "http://192.168.1.201:9000";


class MISTableViewController: UITableViewController {
                            
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let dictURL = NSURL(string: URL_DATA as NSString as String)
        videoDict = NSDictionary(contentsOfURL:dictURL!)
        print(videoDict?.valueForKey("VideoTitle"))
        
        videoArray = videoDict?.objectForKey("Videos") as! NSArray
        print(videoArray.objectAtIndex(0).valueForKey("Title"))
        
        refreshControl?.addTarget(self, action: #selector(MISTableViewController.refreshStart), forControlEvents: UIControlEvents.ValueChanged)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
   
    override func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoArray.count
    }
    
    override func tableView(_: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("videoListCell", forIndexPath: indexPath) as! MISTableViewCell
        
        cell.titleLabel.text = (videoArray.objectAtIndex(indexPath.row).valueForKey("Title")) as? String
        cell.descriptionLabel.text = (videoArray.objectAtIndex(indexPath.row).valueForKey("Description")) as? String
        cell.dateLabel.text = (videoArray.objectAtIndex(indexPath.row).valueForKey("Date")) as? String
        cell.durationLabel.text = (videoArray.objectAtIndex(indexPath.row).valueForKey("Duration")) as? String
        let urlString: NSString = (videoArray.objectAtIndex(indexPath.row).valueForKey("ImageUrl")) as! String
        cell.videoImage.loadMIImageLoaderFromURLString(urlString as String, placeholderImage: UIImage(named: "image_thumbnail")) {
            (finished, error) in
            if finished { }
            else if error != nil {
                print("error occurred with description: \(error.localizedDescription)")
            }
        }
        
        
        print(urlString)
        
        return cell
    }
    
    override func tableView(_: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated:true)

        let videoURL = NSURL(string: (videoArray.objectAtIndex(indexPath.row).valueForKey("VideoUrl")) as! String)
        
        let assetId = videoArray.objectAtIndex(indexPath.row).valueForKey("AssetID") as! String
        let title = videoArray.objectAtIndex(indexPath.row).valueForKey("Title") as! String
        let assetType = videoArray.objectAtIndex(indexPath.row).valueForKey("AssetType") as! String
        
        let player = AVPlayer(URL: videoURL!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        playerController.showsPlaybackControls = true
        playerController.allowsPictureInPicturePlayback = true;

        
        let pair1 = CustomMetadata()
        pair1.key = "AMK1"
        pair1.values = ["AMV1", "AMV11"]
        
        let pair2 = CustomMetadata()
        pair2.key = "CMK1"
        pair2.values = ["CMV1", "CMV11"]
        
        let senseKit = SenseKit();
        senseKit.plugin(forAVPlayer: player, assetId: assetId, serverHost: NEXEVEN_SERVER, nxeCID: "BBQCID", assetType: assetType, assetName: title, viewerId: "jorgenS", assetMetadata: [pair1], viewerMetadata: [pair2])

        playerController.player?.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: #selector(MISTableViewController.playerItemDidReachEnd),
            name: AVPlayerItemDidPlayToEndTimeNotification,
            object: player.currentItem)
        
        self.presentViewController(playerController, animated: true,
            completion: nil)

    
    
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        _ = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MISTableViewController.closeEndVideo), userInfo: nil, repeats: false);
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func closeEndVideo() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func refreshStart(sender: UIRefreshControl!) {
        
        self .viewDidLoad()
        tableView.reloadData()
        _ = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(MISTableViewController.stopRefresh), userInfo: nil, repeats: false)
    
    }
    
    func stopRefresh() {
        
        refreshControl?.endRefreshing()
    }
    
}

