//
//  MISTableViewCell.swift
//  VideoPlayer_swift
//
//  Created by Istvan Szabo on 2014. 09. 24..
//  Copyright (c) 2014. Istvan Szabo. All rights reserved.
//

import UIKit

class MISTableViewCell: UITableViewCell {
    
    @IBOutlet var videoImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
