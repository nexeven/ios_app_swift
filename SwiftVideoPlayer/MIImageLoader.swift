//
//  MIImageLoader.swift
//  VideoPlayer_swift
//
//  Created by Istvan Szabo on 2015. 08. 24..
//  Copyright (c) 2015. Istvan Szabo. All rights reserved.
//


import UIKit
import MapKit
import WatchKit


private var completionHolderAssociationKey: UInt8 = 0
private var indexPathIdentifierAssociationKey: UInt8 = 0
private var controlStateHolderAssociationKey: UInt8 = 0
private var isBackgroundImageAssociationKey: UInt8 = 0

final internal class ControlStateHolder {
    var controlState: UIControlState
    
    init(state: UIControlState) {
        self.controlState = state
    }
}

public extension UIButton {
    
    final internal var indexPathIdentifier: Int! {
        get {
            return objc_getAssociatedObject(self, &indexPathIdentifierAssociationKey) as? Int
        }
        set {
            objc_setAssociatedObject(self, &indexPathIdentifierAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final internal var completionHolder: CompletionHolder! {
        get {
            return objc_getAssociatedObject(self, &completionHolderAssociationKey) as? CompletionHolder
        }
        set {
            objc_setAssociatedObject(self, &completionHolderAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final internal var controlStateHolder: ControlStateHolder! {
        get {
            return objc_getAssociatedObject(self, &controlStateHolderAssociationKey) as? ControlStateHolder
        }
        set {
            objc_setAssociatedObject(self, &controlStateHolderAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final internal var isBackgroundImage: Bool! {
        get {
            return objc_getAssociatedObject(self, &isBackgroundImageAssociationKey) as? Bool
        }
        set {
            objc_setAssociatedObject(self, &isBackgroundImageAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final public func loadImageFromURLString(string: String, placeholderImage: UIImage? = nil, forState controlState: UIControlState = .Normal, isBackgroundImage: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        if let url = NSURL(string: string) {
            loadImageFromURL(url, placeholderImage: placeholderImage, forState: controlState, isBackgroundImage: isBackgroundImage, completion: completion)
        }
    }
    
    final public func loadImageFromURL(url: NSURL, placeholderImage: UIImage? = nil, forState controlState: UIControlState = .Normal, isBackgroundImage: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        let cacheManager = MIImageloader.sharedInstance
        let request = NSMutableURLRequest(URL: url, cachePolicy: cacheManager.session.configuration.requestCachePolicy, timeoutInterval: cacheManager.session.configuration.timeoutIntervalForRequest)
        request.addValue("image/*", forHTTPHeaderField: "Accept")
        loadImageFromRequest(request, placeholderImage: placeholderImage, forState: controlState, isBackgroundImage: isBackgroundImage, completion: completion)
    }
    
    final public func loadImageFromRequest(request: NSURLRequest, placeholderImage: UIImage? = nil, forState controlState: UIControlState = .Normal, isBackgroundImage: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        self.completionHolder = CompletionHolder(completion: completion)
        self.indexPathIdentifier = -1
        self.controlStateHolder = ControlStateHolder(state: controlState)
        self.isBackgroundImage = isBackgroundImage
        
        if request.URL?.absoluteString == nil {
            self.completionHolder.completion?(finished: false, error: nil)
            return
        }
        
        let cacheManager = MIImageloader.sharedInstance
        let fadeAnimationDuration = cacheManager.fadeAnimationDuration
        let urlAbsoluteString = request.URL!.absoluteString!
        
        func loadImage(image: UIImage) -> Void {
            UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                if self.isBackgroundImage == true {
                    self.setBackgroundImage(image, forState: self.controlStateHolder.controlState)
                }
                else {
                    self.setImage(image, forState: self.controlStateHolder.controlState)
                }
                }, completion: nil)
            
            self.completionHolder.completion?(finished: true, error: nil)
        }
        
        if let image = cacheManager[urlAbsoluteString] {
            loadImage(image)
        }
        else if let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request), image = UIImage(data: cachedResponse.data), creationTimestamp = cachedResponse.userInfo?["creationTimestamp"] as? CFTimeInterval where (CACurrentMediaTime() - creationTimestamp) < Double(cacheManager.diskCacheMaxAge) {
            loadImage(image)
            
            cacheManager[urlAbsoluteString] = image
        }
            
        else {
            
            NSURLCache.sharedURLCache().removeCachedResponseForRequest(request)
            
           
            if let image = placeholderImage {
                if self.isBackgroundImage == true {
                    self.setBackgroundImage(image, forState: self.controlStateHolder.controlState)
                }
                else {
                    self.setImage(image, forState: self.controlStateHolder.controlState)
                }
            }
        
            let tableView: UITableView
            let collectionView: UICollectionView
            var tableViewCell: UITableViewCell?
            var collectionViewCell: UICollectionViewCell?
            var parentView = self.superview
            
            while parentView != nil {
                if let view = parentView as? UITableViewCell {
                    tableViewCell = view
                }
                else if let view = parentView as? UITableView {
                    tableView = view
                    
                    if let cell = tableViewCell {
                        let indexPath = tableView.indexPathForRowAtPoint(cell.center)
                        self.indexPathIdentifier = indexPath?.hashValue ?? -1
                    }
                    break
                }
                else if let view = parentView as? UICollectionViewCell {
                    collectionViewCell = view
                }
                else if let view = parentView as? UICollectionView {
                    collectionView = view
                    
                    if let cell = collectionViewCell {
                        let indexPath = collectionView.indexPathForItemAtPoint(cell.center)
                        self.indexPathIdentifier = indexPath?.hashValue ?? -1
                    }
                    break
                }
                
                parentView = parentView?.superview
            }
            
            let initialIndexIdentifier = self.indexPathIdentifier
            
            if cacheManager.isDownloadingFromURL(urlAbsoluteString) == false {
                cacheManager.setIsDownloadingFromURL(true, forURLString: urlAbsoluteString)
                
                let dataTask = cacheManager.session.dataTaskWithRequest(request) {
                    (data: NSData?, response: NSURLResponse?, error: NSError?) in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        var finished = false
                        
                        if error == nil && data != nil {
                            let data: NSData! = data
                            if let image = UIImage(data: data) {
                                if initialIndexIdentifier == self.indexPathIdentifier {
                                    UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                                        if self.isBackgroundImage == true {
                                            self.setBackgroundImage(image, forState: self.controlStateHolder.controlState)
                                        }
                                        else {
                                            self.setImage(image, forState: self.controlStateHolder.controlState)
                                        }
                                        }, completion: nil)
                                }
                                
                                cacheManager[urlAbsoluteString] = image
                                
                                let responseDataIsCacheable = cacheManager.diskCacheMaxAge > 0 &&
                                    Double(data.length) <= 0.05 * Double(NSURLCache.sharedURLCache().diskCapacity) &&
                                    (cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataElseLoad ||
                                        cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataDontLoad) &&
                                    (request.cachePolicy == .ReturnCacheDataElseLoad ||
                                        request.cachePolicy == .ReturnCacheDataDontLoad)
                                
                                if let httpResponse = response as? NSHTTPURLResponse, url = httpResponse.URL where responseDataIsCacheable {
                                    var allHeaderFields: [String:String] = httpResponse.allHeaderFields as! [String:String]
                                    allHeaderFields["Cache-Control"] = "max-age=\(cacheManager.diskCacheMaxAge)"
                                    if let cacheControlResponse = NSHTTPURLResponse(URL: url, statusCode: httpResponse.statusCode, HTTPVersion: "HTTP/1.1", headerFields: allHeaderFields) {
                                        let cachedResponse = NSCachedURLResponse(response: cacheControlResponse, data: data, userInfo: ["creationTimestamp": CACurrentMediaTime()], storagePolicy: .Allowed)
                                        NSURLCache.sharedURLCache().storeCachedResponse(cachedResponse, forRequest: request)
                                    }
                                }
                                
                                finished = true
                            }
                        }
                        
                        if finished == false {
                            cacheManager.setIsDownloadingFromURL(false, forURLString: urlAbsoluteString)
                            cacheManager.removeImageCacheObserversForKey(urlAbsoluteString)
                        }
                        
                        self.completionHolder.completion?(finished: finished, error: error)
                    }
                }
                
                dataTask.resume()
            }else {
                weak var weakSelf = self
                cacheManager.addImageCacheObserver(weakSelf!, withInitialIndexIdentifier: initialIndexIdentifier, forKey: urlAbsoluteString)
            }
        }
    }
}

public extension MKAnnotationView {
    
    final internal var completionHolder: CompletionHolder! {
        get {
            return objc_getAssociatedObject(self, &completionHolderAssociationKey) as? CompletionHolder
        }
        set {
            objc_setAssociatedObject(self, &completionHolderAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final public func loadImageFromURLString(string: String, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        if let url = NSURL(string: string) {
            loadImageFromURL(url, placeholderImage: placeholderImage, completion: completion)
        }
    }
    
    final public func loadImageFromURL(url: NSURL, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        let cacheManager = MIImageloader.sharedInstance
        let request = NSMutableURLRequest(URL: url, cachePolicy: cacheManager.session.configuration.requestCachePolicy, timeoutInterval: cacheManager.session.configuration.timeoutIntervalForRequest)
        request.addValue("image/*", forHTTPHeaderField: "Accept")
        loadImageFromRequest(request, placeholderImage: placeholderImage, completion: completion)
    }
    
    final public func loadImageFromRequest(request: NSURLRequest, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        self.completionHolder = CompletionHolder(completion: completion)
        
        if request.URL?.absoluteString == nil {
            self.completionHolder.completion?(finished: false, error: nil)
            return
        }
        
        let cacheManager = MIImageloader.sharedInstance
        let fadeAnimationDuration = cacheManager.fadeAnimationDuration
        let urlAbsoluteString = request.URL!.absoluteString!
        
        func loadImage(image: UIImage) -> Void {
            UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                self.image = image
                }, completion: nil)
            
            self.completionHolder.completion?(finished: true, error: nil)
        }
        
        if let image = cacheManager[urlAbsoluteString] {
            loadImage(image)
        }
            
        else if let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request), image = UIImage(data: cachedResponse.data), creationTimestamp = cachedResponse.userInfo?["creationTimestamp"] as? CFTimeInterval where (CACurrentMediaTime() - creationTimestamp) < Double(cacheManager.diskCacheMaxAge) {
            loadImage(image)
            
            cacheManager[urlAbsoluteString] = image
        }
           
        else {
    
            NSURLCache.sharedURLCache().removeCachedResponseForRequest(request)
            
                if let image = placeholderImage {
                self.image = image
            }
            
            let initialIndexIdentifier = -1
            
            if cacheManager.isDownloadingFromURL(urlAbsoluteString) == false {
                cacheManager.setIsDownloadingFromURL(true, forURLString: urlAbsoluteString)
                
                let dataTask = cacheManager.session.dataTaskWithRequest(request) {
                    (data: NSData?, response: NSURLResponse?, error: NSError?) in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        var finished = false
                        
                        if error == nil && data != nil {
                            let data: NSData! = data
                            if let image = UIImage(data: data) {
                                UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                                    self.image = image
                                    }, completion: nil)
                                
                                cacheManager[urlAbsoluteString] = image
                                
                                let responseDataIsCacheable = cacheManager.diskCacheMaxAge > 0 &&
                                    Double(data.length) <= 0.05 * Double(NSURLCache.sharedURLCache().diskCapacity) &&
                                    (cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataElseLoad ||
                                        cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataDontLoad) &&
                                    (request.cachePolicy == .ReturnCacheDataElseLoad ||
                                        request.cachePolicy == .ReturnCacheDataDontLoad)
                                
                                if let httpResponse = response as? NSHTTPURLResponse, url = httpResponse.URL where responseDataIsCacheable {
                                    var allHeaderFields: [String:String] = httpResponse.allHeaderFields as! [String:String]
                                    allHeaderFields["Cache-Control"] = "max-age=\(cacheManager.diskCacheMaxAge)"
                                    if let cacheControlResponse = NSHTTPURLResponse(URL: url, statusCode: httpResponse.statusCode, HTTPVersion: "HTTP/1.1", headerFields: allHeaderFields) {
                                        let cachedResponse = NSCachedURLResponse(response: cacheControlResponse, data: data, userInfo: ["creationTimestamp": CACurrentMediaTime()], storagePolicy: .Allowed)
                                        NSURLCache.sharedURLCache().storeCachedResponse(cachedResponse, forRequest: request)
                                    }
                                }
                                
                                finished = true
                            }
                        }
                        
                        if finished == false {
                            cacheManager.setIsDownloadingFromURL(false, forURLString: urlAbsoluteString)
                            cacheManager.removeImageCacheObserversForKey(urlAbsoluteString)
                        }
                        
                        self.completionHolder.completion?(finished: finished, error: error)
                    }
                }
                
                dataTask.resume()
            }else {
                weak var weakSelf = self
                cacheManager.addImageCacheObserver(weakSelf!, withInitialIndexIdentifier: initialIndexIdentifier, forKey: urlAbsoluteString)
            }
        }
    }
}

public extension UIImageView {

    final internal var indexPathIdentifier: Int! {
        get {
            return objc_getAssociatedObject(self, &indexPathIdentifierAssociationKey) as? Int
        }
        set {
            objc_setAssociatedObject(self, &indexPathIdentifierAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final internal var completionHolder: CompletionHolder! {
        get {
            return objc_getAssociatedObject(self, &completionHolderAssociationKey) as? CompletionHolder
        }
        set {
            objc_setAssociatedObject(self, &completionHolderAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    final public func loadMIImageLoaderFromURLString(string: String, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        if let url = NSURL(string: string) {
            loadImageFromURL(url, placeholderImage: placeholderImage, completion: completion)
        }
    }
    
    final public func loadImageFromURL(url: NSURL, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        let cacheManager = MIImageloader.sharedInstance
        let request = NSMutableURLRequest(URL: url, cachePolicy: cacheManager.session.configuration.requestCachePolicy, timeoutInterval: cacheManager.session.configuration.timeoutIntervalForRequest)
        request.addValue("image/*", forHTTPHeaderField: "Accept")
        loadImageFromRequest(request, placeholderImage: placeholderImage, completion: completion)
    }
    
    final public func loadImageFromRequest(request: NSURLRequest, placeholderImage: UIImage? = nil, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        self.completionHolder = CompletionHolder(completion: completion)
        self.indexPathIdentifier = -1
        
        if request.URL?.absoluteString == nil {
            self.completionHolder.completion?(finished: false, error: nil)
            return
        }
        
        let cacheManager = MIImageloader.sharedInstance
        let fadeAnimationDuration = cacheManager.fadeAnimationDuration
        let urlAbsoluteString = request.URL!.absoluteString!
        
        func loadImage(image: UIImage) -> Void {
            UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                self.image = image
                }, completion: nil)
            
            self.completionHolder.completion?(finished: true, error: nil)
        }
        
        
        if let image = cacheManager[urlAbsoluteString] {
            loadImage(image)
        }
        else if let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request), image = UIImage(data: cachedResponse.data), creationTimestamp = cachedResponse.userInfo?["creationTimestamp"] as? CFTimeInterval where (CACurrentMediaTime() - creationTimestamp) < Double(cacheManager.diskCacheMaxAge) {
            loadImage(image)
            
            cacheManager[urlAbsoluteString] = image
        }
           
        else {
            
            NSURLCache.sharedURLCache().removeCachedResponseForRequest(request)
            
            if let image = placeholderImage {
                self.image = image
            }
            
            let tableView: UITableView
            let collectionView: UICollectionView
            var tableViewCell: UITableViewCell?
            var collectionViewCell: UICollectionViewCell?
            var parentView = self.superview
            
            while parentView != nil {
                if let view = parentView as? UITableViewCell {
                    tableViewCell = view
                }
                else if let view = parentView as? UITableView {
                    tableView = view
                    
                    if let cell = tableViewCell {
                        let indexPath = tableView.indexPathForRowAtPoint(cell.center)
                        self.indexPathIdentifier = indexPath?.hashValue ?? -1
                    }
                    break
                }
                else if let view = parentView as? UICollectionViewCell {
                    collectionViewCell = view
                }
                else if let view = parentView as? UICollectionView {
                    collectionView = view
                    
                    if let cell = collectionViewCell {
                        let indexPath = collectionView.indexPathForItemAtPoint(cell.center)
                        self.indexPathIdentifier = indexPath?.hashValue ?? -1
                    }
                    break
                }
                
                parentView = parentView?.superview
            }
            
            let initialIndexIdentifier = self.indexPathIdentifier
            
                if cacheManager.isDownloadingFromURL(urlAbsoluteString) == false {
                cacheManager.setIsDownloadingFromURL(true, forURLString: urlAbsoluteString)
                
                let dataTask = cacheManager.session.dataTaskWithRequest(request) {
                    (data: NSData?, response: NSURLResponse?, error: NSError?) in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        var finished = false
                        if error == nil && data != nil {
                            let data: NSData! = data
                            if let image = UIImage(data: data) {
                                if initialIndexIdentifier == self.indexPathIdentifier {
                                    UIView.transitionWithView(self, duration: fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                                        self.image = image
                                        }, completion: nil)
                                }
                                
                                cacheManager[urlAbsoluteString] = image
                                
                                let responseDataIsCacheable = cacheManager.diskCacheMaxAge > 0 &&
                                    Double(data.length) <= 0.05 * Double(NSURLCache.sharedURLCache().diskCapacity) &&
                                    (cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataElseLoad ||
                                        cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataDontLoad) &&
                                    (request.cachePolicy == .ReturnCacheDataElseLoad ||
                                        request.cachePolicy == .ReturnCacheDataDontLoad)
                                
                                if let httpResponse = response as? NSHTTPURLResponse, url = httpResponse.URL where responseDataIsCacheable {
                                    var allHeaderFields: [String:String] = httpResponse.allHeaderFields as! [String:String]
                                    allHeaderFields["Cache-Control"] = "max-age=\(cacheManager.diskCacheMaxAge)"
                                    if let cacheControlResponse = NSHTTPURLResponse(URL: url, statusCode: httpResponse.statusCode, HTTPVersion: "HTTP/1.1", headerFields: allHeaderFields) {
                                        let cachedResponse = NSCachedURLResponse(response: cacheControlResponse, data: data, userInfo: ["creationTimestamp": CACurrentMediaTime()], storagePolicy: .Allowed)
                                        NSURLCache.sharedURLCache().storeCachedResponse(cachedResponse, forRequest: request)
                                    }
                                }
                                
                                finished = true
                            }
                        }
                        
                        if finished == false {
                            cacheManager.setIsDownloadingFromURL(false, forURLString: urlAbsoluteString)
                            cacheManager.removeImageCacheObserversForKey(urlAbsoluteString)
                        }
                        
                        self.completionHolder.completion?(finished: finished, error: error)
                    }
                }
                
                dataTask.resume()
            }else {
                weak var weakSelf = self
                cacheManager.addImageCacheObserver(weakSelf!, withInitialIndexIdentifier: initialIndexIdentifier, forKey: urlAbsoluteString)
            }
        }
    }
}


public extension WKInterfaceImage {
    
    final internal var completionHolder: CompletionHolder! {
        get {
            return objc_getAssociatedObject(self, &completionHolderAssociationKey) as? CompletionHolder
        }
        set {
            objc_setAssociatedObject(self, &completionHolderAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
    final private func loadImageFromData(imageData: NSData) -> Void {
        self.setImageData(imageData)
    }
    
    final private func storeImageDataInDeviceCache(imageData: NSData, forURLAbsoluteString urlAbsoluteString: String) {
        
        let maxCacheSize = 5 * 1024 * 1024
        var cacheTotalCost = imageData.length
        let currentDevice = WKInterfaceDevice.currentDevice()
        
        
        if cacheTotalCost > maxCacheSize {
            loadImageFromData(imageData)
        }
        else {
            for (urlString, cacheCostNumber) in currentDevice.cachedImages {
                cacheTotalCost += cacheCostNumber.integerValue
                
                
                if cacheTotalCost > maxCacheSize {
                    currentDevice.removeCachedImageWithName(urlString)
                }
            }
            
            if currentDevice.addCachedImageWithData(imageData, name: urlAbsoluteString) {
                self.setImageNamed(urlAbsoluteString)
            }
            else {
                loadImageFromData(imageData)
            }
        }
    }
    
    final public func loadImageFromURLString(string: String, placeholderImageName: String? = nil, shouldUseDeviceCache: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        if let url = NSURL(string: string) {
            loadImageFromURL(url, placeholderImageName: placeholderImageName, shouldUseDeviceCache: shouldUseDeviceCache, completion: completion)
        }
    }
    
    final public func loadImageFromURL(url: NSURL, placeholderImageName: String? = nil, shouldUseDeviceCache: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        let cacheManager = MIImageloader.sharedInstance
        let request = NSMutableURLRequest(URL: url, cachePolicy: cacheManager.session.configuration.requestCachePolicy, timeoutInterval: cacheManager.session.configuration.timeoutIntervalForRequest)
        request.addValue("image/*", forHTTPHeaderField: "Accept")
        loadImageFromRequest(request, placeholderImageName: placeholderImageName, shouldUseDeviceCache: shouldUseDeviceCache, completion: completion)
    }
    
    final public func loadImageFromRequest(request: NSURLRequest, placeholderImageName: String? = nil, shouldUseDeviceCache: Bool = false, completion: ((finished: Bool, error: NSError!) -> Void)? = nil) {
        self.completionHolder = CompletionHolder(completion: completion)
        
        if request.URL?.absoluteString == nil {
            self.completionHolder.completion?(finished: false, error: nil)
            return
        }
        
        let cacheManager = MIImageloader.sharedInstance
        _ = cacheManager.fadeAnimationDuration
        let urlAbsoluteString: String! = request.URL!.absoluteString
        let initialIndexIdentifier = -1
        let currentDevice = WKInterfaceDevice.currentDevice()
        
        if shouldUseDeviceCache {
            
            if currentDevice.cachedImages[urlAbsoluteString] != nil {
                self.setImageNamed(urlAbsoluteString)
                self.completionHolder.completion?(finished: true, error: nil)
                return
            }
        }
        else {
            currentDevice.removeCachedImageWithName(urlAbsoluteString)
        }
        
        if let image = cacheManager[urlAbsoluteString] {
            let imageData: NSData! = UIImagePNGRepresentation(image)
            
            if shouldUseDeviceCache {
                storeImageDataInDeviceCache(imageData, forURLAbsoluteString: urlAbsoluteString)
            }
            else {
                loadImageFromData(imageData)
            }
            
            self.completionHolder.completion?(finished: true, error: nil)
        }
        else if let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request), image = UIImage(data: cachedResponse.data), creationTimestamp = cachedResponse.userInfo?["creationTimestamp"] as? CFTimeInterval where (CACurrentMediaTime() - creationTimestamp) < Double(cacheManager.diskCacheMaxAge) {
            if shouldUseDeviceCache {
                storeImageDataInDeviceCache(cachedResponse.data, forURLAbsoluteString: urlAbsoluteString)
            }
            else {
                loadImageFromData(cachedResponse.data)
            }
            
            cacheManager[urlAbsoluteString] = image
            self.completionHolder.completion?(finished: true, error: nil)
        }
        else {
            
            NSURLCache.sharedURLCache().removeCachedResponseForRequest(request)
            
            if let imageName = placeholderImageName {
                self.setImageNamed(imageName)
            }
            
            if cacheManager.isDownloadingFromURL(urlAbsoluteString) == false {
                cacheManager.setIsDownloadingFromURL(true, forURLString: urlAbsoluteString)
                
                let dataTask = cacheManager.session.dataTaskWithRequest(request) {
                    (data: NSData?, response: NSURLResponse?, error: NSError?) in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        var finished = false
                        if error == nil && data != nil {
                            let data: NSData! = data
                            if let image = UIImage(data: data) {
                                if shouldUseDeviceCache {
                                    self.storeImageDataInDeviceCache(data, forURLAbsoluteString: urlAbsoluteString)
                                }
                                else {
                                    self.setImage(image)
                                }
                                
                                cacheManager[urlAbsoluteString] = image
                                
                                let responseDataIsCacheable = cacheManager.diskCacheMaxAge > 0 &&
                                    Double(data.length) <= 0.05 * Double(NSURLCache.sharedURLCache().diskCapacity) &&
                                    (cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataElseLoad ||
                                        cacheManager.session.configuration.requestCachePolicy == .ReturnCacheDataDontLoad) &&
                                    (request.cachePolicy == .ReturnCacheDataElseLoad ||
                                        request.cachePolicy == .ReturnCacheDataDontLoad)
                                
                                if let httpResponse = response as? NSHTTPURLResponse, url = httpResponse.URL where responseDataIsCacheable {
                                    var allHeaderFields: [String: String] = httpResponse.allHeaderFields as! [String: String]
                                    allHeaderFields["Cache-Control"] = "max-age=\(cacheManager.diskCacheMaxAge)"
                                    if let cacheControlResponse = NSHTTPURLResponse(URL: url, statusCode: httpResponse.statusCode, HTTPVersion: "HTTP/1.1", headerFields: allHeaderFields) {
                                        let cachedResponse = NSCachedURLResponse(response: cacheControlResponse, data: data, userInfo: ["creationTimestamp": CACurrentMediaTime()], storagePolicy: .Allowed)
                                        NSURLCache.sharedURLCache().storeCachedResponse(cachedResponse, forRequest: request)
                                    }
                                }
                                
                                finished = true
                            }
                        }
                        
                        if finished == false {
                            cacheManager.setIsDownloadingFromURL(false, forURLString: urlAbsoluteString)
                            cacheManager.removeImageCacheObserversForKey(urlAbsoluteString)
                        }
                        
                        self.completionHolder.completion?(finished: finished, error: error)
                    }
                }
                
                dataTask.resume()
            }
            else {
                weak var weakSelf = self
                cacheManager.addImageCacheObserver(weakSelf!, withInitialIndexIdentifier: initialIndexIdentifier, forKey: urlAbsoluteString)
            }
        }
    }
}


final internal class CompletionHolder {
    var completion: ((finished: Bool, error: NSError!) -> Void)?
    
    init(completion: ((Bool, error: NSError!) -> Void)?) {
        self.completion = completion
    }
}


final public class MIImageloader {
    
    private struct ImageCacheKeys {
        static let img = "img"
        static let isDownloading = "isDownloading"
        static let observerMapping = "observerMapping"
    }
    
    public static let sharedInstance = MIImageloader()
    
    private var imageCache = [String: [String: AnyObject]]()
    
    internal let session: NSURLSession = {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.requestCachePolicy = .ReturnCacheDataElseLoad
        configuration.URLCache = .sharedURLCache()
        return NSURLSession(configuration: configuration)
    }()
    
       public var fadeAnimationDuration: NSTimeInterval = 0.1
    
   
    public var diskCacheMaxAge: UInt = 60 * 60 * 24 * 7 {
        willSet {
            if newValue == 0 {
                NSURLCache.sharedURLCache().removeAllCachedResponses()
            }
        }
    }
    
   
    
    public var timeoutIntervalForRequest: NSTimeInterval = 60.0 {
        willSet {
            self.session.configuration.timeoutIntervalForRequest = newValue
        }
    }
    
       public var requestCachePolicy: NSURLRequestCachePolicy = .ReturnCacheDataElseLoad {
        willSet {
            self.session.configuration.requestCachePolicy = newValue
        }
    }
    
    private init() {
       
        let diskURLCache = NSURLCache(memoryCapacity: 0, diskCapacity: 50 * 1024 * 1024, diskPath: nil)
        NSURLCache.setSharedURLCache(diskURLCache)
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidReceiveMemoryWarningNotification, object: nil, queue: NSOperationQueue.mainQueue()) {
            _ in
            
            self.imageCache.removeAll(keepCapacity: false)
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
        internal subscript (key: String) -> UIImage? {
        get {
            return imageCacheEntryForKey(key)[ImageCacheKeys.img] as? UIImage
        }
        set {
            if let image = newValue {
                var imageCacheEntry = imageCacheEntryForKey(key)
                imageCacheEntry[ImageCacheKeys.img] = image
                setImageCacheEntry(imageCacheEntry, forKey: key)
                
                if let observerMapping = imageCacheEntry[ImageCacheKeys.observerMapping] as? [NSObject: Int] {
                    for (observer, initialIndexIdentifier) in observerMapping {
                        switch observer {
                        case let imageView as UIImageView:
                            loadObserverAsImageView(imageView, forImage: image, withInitialIndexIdentifier: initialIndexIdentifier)
                        case let button as UIButton:
                            loadObserverAsButton(button, forImage: image, withInitialIndexIdentifier: initialIndexIdentifier)
                        case let annotationView as MKAnnotationView:
                            loadObserverAsAnnotationView(annotationView, forImage: image)
                        case let interfaceImage as WKInterfaceImage:
                            loadObserverAsInterfaceImage(interfaceImage, forImage: image, withKey: key)
                        default:
                            break
                        }
                    }

                    removeImageCacheObserversForKey(key)
                }
            }
        }
    }
    
    
    internal func imageCacheEntryForKey(key: String) -> [String: AnyObject] {
        if let imageCacheEntry = self.imageCache[key] {
            return imageCacheEntry
        }
        else {
            let imageCacheEntry: [String: AnyObject] = [ImageCacheKeys.isDownloading: false, ImageCacheKeys.observerMapping: [NSObject: Int]()]
            self.imageCache[key] = imageCacheEntry
            return imageCacheEntry
        }
    }
    
    internal func setImageCacheEntry(imageCacheEntry: [String: AnyObject], forKey key: String) {
        self.imageCache[key] = imageCacheEntry
    }
    
    internal func isDownloadingFromURL(urlString: String) -> Bool {
        let isDownloading = imageCacheEntryForKey(urlString)[ImageCacheKeys.isDownloading] as? Bool
        
        return isDownloading ?? false
    }
    
    internal func setIsDownloadingFromURL(isDownloading: Bool, forURLString urlString: String) {
        var imageCacheEntry = imageCacheEntryForKey(urlString)
        imageCacheEntry[ImageCacheKeys.isDownloading] = isDownloading
        setImageCacheEntry(imageCacheEntry, forKey: urlString)
    }
    
    internal func addImageCacheObserver(observer: NSObject, withInitialIndexIdentifier initialIndexIdentifier: Int, forKey key: String) {
        var imageCacheEntry = imageCacheEntryForKey(key)
        if var observerMapping = imageCacheEntry[ImageCacheKeys.observerMapping] as? [NSObject: Int] {
            observerMapping[observer] = initialIndexIdentifier
            imageCacheEntry[ImageCacheKeys.observerMapping] = observerMapping
            setImageCacheEntry(imageCacheEntry, forKey: key)
        }
    }
    
    internal func removeImageCacheObserversForKey(key: String) {
        var imageCacheEntry = imageCacheEntryForKey(key)
        if var observerMapping = imageCacheEntry[ImageCacheKeys.observerMapping] as? [NSObject: Int] {
            observerMapping.removeAll(keepCapacity: false)
            imageCacheEntry[ImageCacheKeys.observerMapping] = observerMapping
            setImageCacheEntry(imageCacheEntry, forKey: key)
        }
    }
    
    internal func loadObserverAsImageView(observer: UIImageView, forImage image: UIImage, withInitialIndexIdentifier initialIndexIdentifier: Int) {
        if initialIndexIdentifier == observer.indexPathIdentifier {
            dispatch_async(dispatch_get_main_queue()) {
                UIView.transitionWithView(observer, duration: self.fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                    observer.image = image
                }, completion: nil)
                
                observer.completionHolder.completion?(finished: true, error: nil)
            }
        }
        else {
            observer.completionHolder.completion?(finished: false, error: nil)
        }
    }
    
    internal func loadObserverAsButton(observer: UIButton, forImage image: UIImage, withInitialIndexIdentifier initialIndexIdentifier: Int) {
        if initialIndexIdentifier == observer.indexPathIdentifier {
            dispatch_async(dispatch_get_main_queue()) {
                UIView.transitionWithView(observer, duration: self.fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                    if observer.isBackgroundImage == true {
                        observer.setBackgroundImage(image, forState: observer.controlStateHolder.controlState)
                    }
                    else {
                        observer.setImage(image, forState: observer.controlStateHolder.controlState)
                    }
                }, completion: nil)
                
                observer.completionHolder.completion?(finished: true, error: nil)
            }
        }
        else {
            observer.completionHolder.completion?(finished: false, error: nil)
        }
    }
    
    internal func loadObserverAsAnnotationView(observer: MKAnnotationView, forImage image: UIImage) {
        dispatch_async(dispatch_get_main_queue()) {
            UIView.transitionWithView(observer, duration: self.fadeAnimationDuration, options: .TransitionCrossDissolve, animations: {
                observer.image = image
            }, completion: nil)
            
            observer.completionHolder.completion?(finished: true, error: nil)
        }
    }
    
    internal func loadObserverAsInterfaceImage(observer: WKInterfaceImage, forImage image: UIImage, withKey key: String) {
        dispatch_async(dispatch_get_main_queue()) {
            if WKInterfaceDevice.currentDevice().cachedImages[key] != nil {
                observer.setImageNamed(key)
            }
            else {
                observer.setImageData(UIImagePNGRepresentation(image))
            }
            
            observer.completionHolder.completion?(finished: true, error: nil)
        }
    }
}
